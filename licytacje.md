##1. Scenariusz realizacji przypadku użycia: Licytacja ofert

Warunki początkowe:

    1. Klient jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Użytkownik wybiera interesującą go ofertę
    2. Użytkownik klika link "licytuj"
    3. System przedstawia użytkownikowi formularz, zawierający pole do wpisywania kwoty i przycisk "Licytuj"
    4. Użytkownik wpisuje kwotę w formularzu i klika "Licytuj"
    5. System wyświetla komunikat: "Czy na pewno chcesz wziąć udział w licytacji?" i pokazuje wpisaną uprzednio kwotę
    6. Użytkownik klika przycisk "Tak"
    7. System zapisuje użytkownika w aukcji
    8. System przekierowuje użytkownika na poprzednią podstronę
Scenariusz alternatywny:

    6a. Użytkownik klika przycisk "Nie"
    7a. System ignoruje żądanie użytkownika

##2. Scenariusz realizacji przypadku użycia: Wycofanie oferty

Warunki początkowe:

    1. Autor jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Autor wybiera interesującą go ofertę
    2. Autor klika link "wycofaj"
    3. System wyświetla komunikat: "Czy na pewno?"
    4. Autor klika przycisk "Tak"
    5. System wysyła powiadomienia do klientów biorących udział w licytacji o zaistniałym zdarzeniu
    6. System w bazie danych oznacza ofertę jako wycofaną
    7. System przekierowuje autora na poprzednią podstronę
Scenariusz alternatywny:

    4a. Autor klika przycisk "Nie"
    5a. System ignoruje żądanie autora
    6a. System ignoruje żądanie autora
    
##3. Scenariusz realizacji przypadku użycia: Prośba o wycofanie oferty

Warunki początkowe:

    1. Klient jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Klient wybiera interesującą go ofertę
    2. Klient klika link "wycofaj"
    3. System wyświetla komunikat: "Na pewno?"
    4. Klient klika przycisk "Tak"
    5. System wysyła powiadomienia do autora oferty o zaistniałym zdarzeniu
    6. System przekierowuje autora na poprzednią podstronę
Scenariusz alternatywny:

    4a. Autor klika przycisk "Nie"
    5a. System ignoruje żądanie autora
    
##3a. Scenariusz realizacji przypadku użycia: Prośba o wycofanie oferty

Warunki początkowe:

    1. Administrator jest zalogowany
    2. System posiada połączenie z bazą danych
    3. Autor oferty wyraził zgodę na wycofanie
Scenariusz główny:

    1. Administrator wybiera ofertę
    2. Administrator klika link "wycofaj"
    3. System wyświetla komunikat: "Na pewno?"
    4. Administrator klika przycisk "Tak"
    5. System oznacza w bazie danych ofertę jako wycofaną
    6. System przekierowuje administratora na poprzednią podstronę
Scenariusz alternatywny:

    4a. Administrator klika przycisk "Nie"
    5a. System ignoruje żądanie administratora