##1. Scenariusz realizacji przypadku użycia: Wyszukiwanie

Warunki początkowe:

    1. Użytkownik jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Użytkownik przechodzi na stronę wyszukiwania
    2. System przedstawia użytkownikowi formularz z wyborem kryteriów wyszukiwania oraz spis tagów
    3. Użytkownik wpisuje kryteria i klika przycisk "Szukaj"
    4. System sprawdza w bazie danych istniejące oferty na podstawie kryteriów
    5. System zwraca wyniki wyszukiwania do użytkownika
    6. Użytkownik może kliknąć przycisk "Powrót" by wrócić na poprzednią stronę
Scenariusz alternatywny:

    3a. Użytkownik klika wybrany tag

##2. Scenariusz realizacji przypadku użycia: Wyświetlanie mapy

Warunki początkowe:

    1. Użytkownik jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Użytkownik przechodzi na stronę map