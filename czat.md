##1. Scenariusz realizacji przypadku użycia: Dodawanie wiadomości

Warunki początkowe:

    1. Użytkownik jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Użytkownik szuka formularza na stronie
    2. Użytkownik wpisuje wiadomość w formularzu
    3. Użytkownik klika przycisk "Dodaj wiadomość"
    4. System wyświetla komunikat: "Dodano wiadomość"
    5. Użytkownik zostaje przekierowany na stronę na której był przed operacją

##2. Scenariusz realizacji przypadku użycia: Edytowanie wiadomości

Warunki początkowe:

    1. Użytkownik/Administrator jest zalogowany
    2. System posiada połączenie z bazą danych
    3. Użytkownik posiada uprawnienia do edycji swoich wiadomości
Scenariusz główny:

    1. Użytkownik/Administrator przechodzi na podstronę czatu
    2. Użytkownik/Administrator klika link "edytuj wiadomość"
    3. System przekierowuje Użytkownika/Administratora na podstronę edycji wiadomości
    4. Użytkownik/Administrator wprowadza korekty
    5. Użytkownik/Administrator klika link "Zapisz zmiany"
    6. System wyświetla komunikat: "Wprowadzono zmiany"
    7. Użytkownik/Administrator może powrócić na poprzednią podstronę
Scenariusz alternatywny:

    4a. Użytkownik/Administrator nie wprowadza zmian
    6a. System wyświetla komunikat: "Nie wprowadzono żadnych zmian"
    
##3. Scenariusz realizacji przypadku użycia: Usuwanie wiadomości

Warunki początkowe:

    1. Administrator jest zalogowany
    2. System posiada połączenie z bazą danych
Scenariusz główny:

    1. Administrator przechodzi na podstronę czatu
    2. Administrator klika link "usuń wiadomość"
    3. System tworzy formularz z przyciskami "Tak" i "Nie", komunikatem: "Czy na pewno chcesz usunąć tę wiadomość?" i polem "Powód usunięcia"
    4. Administrator w polu "Powód usunięcia" wpisuje powód i klika przycisk "Tak"
    5. System usuwa wiadomość z bazy danych
    6. System wysyła powiadomienie do właściciela wiadomości o usunięciu jego wiadomości wraz z powodem
    7. Administrator zostaje przekierowany na poprzednią podstronę
Scenariusz alternatywny:

    4a. Administrator klika przycisk "Nie"
    5a. System nie usuwa wiadomości
    6a. Nie jest wysyłane powiadomienie